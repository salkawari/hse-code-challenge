# HSE24 Coding Challenge Solution

This is my solution the the hse24 coding challenge. I have understood that any tech-stack is allowed to solve this code
challenge. To this end, i have chosen to use:

* java11
* spring webflux, reactive spring boot
* mongodb
* angular

## Getting Started
This repo is [here](https://bitbucket.org/salkawari/hse-code-challenge/src/master/), just clone it to get a copy.

### Prerequisites
Here is a list of tools you will need to have installed locally before you can build this project. I have included the 
version I currently have installed:

* java 11
* maven (3.6.0)
* npm (6.9.0)
* node (10.16.3)
* docker (19.03.5)
* a fixer api access key (from www.fixer.io)

<br/>

If you use intellij, you will need to install the lombok plugin. Additionally, you will need to change the javascript
language level to the ECMAScript 6.

### Installing
Make the fixer api access key available to the application as an environment variable:
```
export FIXER_KEY=av3d..
```

To build this project from the command line:
```
mvn clean package
```

## Execution
We need to start the mongo database and then start the application to get us up and running.

### Starting MongoDB Locally
To start a local copy of mongodb call the following from the root of this project:
```
cd local
docker-compose up
```
This will create volumes locally in a subfolder called mongo-volume, which is used to make the data persistent.

### Starting the application locally
The jar build in app-service/target (app-service-1.0.0-SNAPSHOT.jar) can be executed, from the root of this project:
```
java -jar app-service/target/*.jar
```
It might take 10 seconds to get everything running, depending on your hardware.

Now you can see the frontend of the application by going to http://localhost:8080/ in a browser (I'm using the 
latest chrome).

## User Guide
[Here](USERGUIDE.md) is the user guide.

### Stopping MongoDB Locally
To stop a local copy of mongodb call the following:
```
cd local
docker-compose down
```

To stop the java application, just press control + c on the command line where you started it.

## Acknowledgments
#### Joining angular to springboot via maven
I used this [tutorial](https://dzone.com/articles/simplified-building-web-application-using-spring-b) to combine spring 
boot with angular via maven.

#### docker compose for mongodb 
I used this [tutorial](https://medium.com/faun/managing-mongodb-on-docker-with-docker-compose-26bf8a0bbae3) to get mongodb
up and running via docker compose.
