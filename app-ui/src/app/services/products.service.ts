import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import {Observable} from 'rxjs';

@Injectable()
export abstract class PrdSvc {
  abstract getAllCategories();
  abstract getCategory(uuid: string);
  abstract createNewCategory(name: string);
  abstract createNewCategory(name: string);
  abstract deleteCategory(uuid: any);
  abstract getAllProducts();
  abstract getProduct(uuid: string);
  abstract createNewProduct(name: string, categoryId: string, euroPrice: number);
  abstract deleteProduct(uuid: string);
  abstract changeProductDetails(productIs: string, productName: string, categoryId: string, euroPrice: number);
  abstract changeCategoryName(categoryId: string, categoryName: string);
  abstract getAllCurrencies();
  abstract convertfromEuro(euroPrice, exchangeRate): number;
  abstract convertToEuro(otherCurrencyPrice, exchangeRate): number;
  abstract defaultDialogCurrency(selectedCurrency: string): any;
}

@Injectable({
  providedIn: 'root'
})
export class ProductsService implements PrdSvc {

   private $categories;

  constructor(private apiSvc: ApiService) {}

  getAllCategories() {
    return this.apiSvc.get('categories/');
  }

  getCategory(uuid: string) {
    return this.apiSvc.get('categories/' + uuid);
  }

  createNewCategory(name: string) {
    return this.apiSvc.post('categories/', {
      categoryName: name
    });
  }

  deleteCategory(uuid: any) {
    return this.apiSvc.delete('categories/' + uuid);
  }

  getAllProducts() {
    return this.apiSvc.get('products/');
  }

  getProduct(uuid: string) {
    return this.apiSvc.get('products/' + uuid);
  }

  createNewProduct(name: string, categoryId: string, euroPrice: number) {
    const data =  {
      categoryId,
      productName: name,
      euroPrice
    };
    return this.apiSvc.post('products/', data);
  }

  deleteProduct(uuid: string) {
    return this.apiSvc.delete('products/' + uuid);
  }

  changeProductDetails(productIs: string, productName: string, categoryId: string, euroPrice: number) {
    const data =  {
      categoryId,
      productName,
      euroPrice
    };
    return this.apiSvc.put('products/' + productIs, data);
  }

  changeCategoryName(categoryId: string, categoryName: string) {
    return this.apiSvc.put('categories/' + categoryId, {categoryName});
  }

  getAllCurrencies() {
    return this.apiSvc.get('currencies');
  }

  convertfromEuro(euroPrice, exchangeRate): number {
    return (euroPrice * exchangeRate);
  }

  convertToEuro(otherCurrencyPrice, exchangeRate): number {
    return (otherCurrencyPrice / exchangeRate);
  }

  defaultDialogCurrency(selectedCurrency: string): any {
    switch (selectedCurrency) {
      case 'USD':
          return '$';
      case 'GBP':
          return '£';
      default:
        return '€';
    }
  }
}
