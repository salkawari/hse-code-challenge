import { Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

export enum RequestMethod {
    Get = 'get',
    Post = 'post',
    Put = 'put',
    Delete = 'delete',
    Options = 'options',
    Head = 'head',
    Patch = 'patch'
  }

@Injectable({
    providedIn: 'root'
})
export class ApiService implements HttpInterceptor {
    public api = "http://localhost:8080/";

    constructor(private http: HttpClient) {}

    init(api: string): void {
        this.api = api;
    }

    fetch<T>(method: RequestMethod, path, ...args): Observable<T> {
        const url = this.api + path;
        let headers = new HttpHeaders();
        headers = headers.append('x-Flatten', 'true');
        headers = headers.append('Content-Type', 'application/json; charset=utf-8');
        args.push({headers});
        return this.http[method.toString()](url, ...args );
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(null, error => {
                if (error.status === 0) {
                   // backend not available -> emit error
                } else  {
                    console.log("error", request);
                }
            })
        );
    }

    post    <T>(path: string, data): Observable<T> { return this.fetch(RequestMethod.Post, path, data); }
    get     <T>(path: string): Observable<T>       { return this.fetch(RequestMethod.Get, path); }
    put     <T>(path: string, data): Observable<T> { return this.fetch(RequestMethod.Put, path, data); }
    delete  <T>(path: string): Observable<T>       { return this.fetch(RequestMethod.Delete, path); }
}