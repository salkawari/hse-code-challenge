import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent} from './components/header/header.component';
import { AsideComponent } from './components/aside/aside.component';
import { ProductViewComponent } from './components/product-view/product-view.component';
import { DialogComponent } from './components/dialog/dialog.component';

import { ConvertFromEuroPipe } from './pipes/convertFromEuro.pipe';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { ShareButtonsModule } from '@ngx-share/buttons';
import {PrdSvc, ProductsService} from "./services/products.service";



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProductViewComponent,
    AsideComponent,
    DialogComponent,
    ConvertFromEuroPipe
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatListModule,
    MatDialogModule,
    MatFormFieldModule,
    FontAwesomeModule,
    MatInputModule,
    MatSelectModule,
    ShareButtonsModule
  ],
  bootstrap: [AppComponent],
  providers: [ { provide: PrdSvc, useClass: ProductsService } ],
  entryComponents: [
    DialogComponent
  ]
})
export class AppModule { }
