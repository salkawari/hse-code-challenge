import { Component } from '@angular/core';
import {PrdSvc, ProductsService} from './services/products.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ { provide: PrdSvc, useClass: ProductsService } ],
})
export class AppComponent {

  title = 'app-ui';
  allCategories$;
  allProducts$;
  allCurrencies$ = {
    EUR: 1,
    GBP: null,
    USD: null
  };
  currentCategory;
  currentCurrency;
  currentExchangeRate

  constructor(private prodSvc: PrdSvc) { }

  ngOnInit() {
    this.updateView();
    this.updateCurrency();
    this.prodSvc.getAllCurrencies().subscribe(result => {
      this.allCurrencies$.GBP = (result as any).rates.GBP;
      this.allCurrencies$.USD = (result as any).rates.USD;
    })
  }

  setCurrentCategory(categoryId: string) {
    this.currentCategory = categoryId;
  }

  updateView() {
   this.prodSvc.getAllCategories().subscribe(result => {
      this.allCategories$ = result;

      this.prodSvc.getAllProducts().subscribe(result => {
        this.allProducts$ = result;
      });
    });
  }

  updateCurrency(curr: string = 'EUR') {
    this.currentCurrency = curr;
    Object.keys(this.allCurrencies$).forEach((key) => {
      if(key === this.currentCurrency) {

        this.currentExchangeRate =  this.allCurrencies$[key];
      }
  });
  }
}
