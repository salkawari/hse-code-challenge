import { Component, OnInit, Inject, Output, EventEmitter, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogComponent, DialogData } from '../dialog/dialog.component';
import {PrdSvc, ProductsService} from 'src/app/services/products.service';
import { LowerCasePipe } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  // providers: [ LowerCasePipe ]
  providers: [ LowerCasePipe, { provide: PrdSvc, useClass: ProductsService } ]
})
export class HeaderComponent implements OnInit {

  @Input() allCurrencies$;

  @Output() update: EventEmitter<null> = new EventEmitter();
  @Output() updateCurrency: EventEmitter<string> = new EventEmitter();

  allCurrencies = ['EUR', 'USD', 'GBP'];
  selectedCurrency = 'EUR';

  constructor(public dialog: MatDialog, private prodSvc: PrdSvc, private lowercase: LowerCasePipe) { }

  ngOnInit() {
  }

  createCategoryDialog() {

    const dialogRef = this.dialog.open(DialogComponent, {
      width: '300px',
      data: {
        type: 'createCategory',
        title: 'Create new category...',
        text: 'Give the new category a name.',
        inputAValue: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== null && result !== undefined) {
        console.log('The dialog was closed');
        let duplicate = false;
        this.prodSvc.getAllCategories().subscribe(categories => {
          (categories as any).forEach(element => {
            if (this.lowercase.transform(element.categoryName) === this.lowercase.transform(result)) {
              console.log('category already exists');
              duplicate = true;

              const dialogSecindaryRef = this.dialog.open(DialogComponent, {
                width: '300px',
                data: {
                  type: 'duplicateName',
                  title: 'Category already exists.....',
                  text: 'A Category with the name ' + result + ' already exists.'
                }
              });
            }
          });

          if (!duplicate) {
            this.prodSvc.createNewCategory(result).subscribe(_ => {
              this.update.emit();
            });
          }
        });
      }
    });
  }

  createProductDialog() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        type: 'createProduct',
        title: 'Create new product...',
        text: 'Give the new product a name.',
        textB: 'Which category does it belong to?',
        textC: 'What is its price?',
        inputAValue: '',
        inputBValue: '',
        inputCValue: 0,
        curr: this.prodSvc.defaultDialogCurrency(this.selectedCurrency)
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== null && result !== undefined) {
        console.log('The dialog was closed', result);
        let euroPrice = result[2];

        if (result[3] !== '€') {
          switch (result[3]) {
            case '$':
              euroPrice = this.prodSvc.convertToEuro(result[2], this.allCurrencies$.USD);
              break;

            case '£':
              euroPrice = this.prodSvc.convertToEuro(result[2], this.allCurrencies$.GBP);
              break;

            default:
              break;
          }
        }

        this.prodSvc.createNewProduct(result[0], result[1], euroPrice).subscribe(_ => {
          this.update.emit();
        });
      }

    });
  }

  selectCurrency() {
    this.updateCurrency.emit(this.selectedCurrency);
  }
}
