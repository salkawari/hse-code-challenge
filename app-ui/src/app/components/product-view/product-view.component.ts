import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import {PrdSvc, ProductsService} from 'src/app/services/products.service';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss'],
  providers: [ { provide: PrdSvc, useClass: ProductsService } ]
})
export class ProductViewComponent implements OnInit, OnChanges {

  constructor(public dialog: MatDialog, private prodSvc: PrdSvc) { }

  @Input() allProducts$;
  @Input() currentCategory = 'all';
  @Input() currentCurrency;
  @Input() currentExchangeRate;
  @Input() allCurrencies$;

  @Output() update: EventEmitter<null> = new EventEmitter();

  currentProducts = [];
  // FA-Icons
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;

  private readonly dialogWasClosed = 'The dialog was closed';

  ngOnInit() { }

  ngOnChanges() {
    this.fillCurrentProducts();
  }

  fillCurrentProducts() {
    if (this.currentCategory === 'all') {
      this.currentProducts = this.allProducts$;
    } else {
      this.currentProducts = [];
      this.allProducts$.forEach(product => {
        if (product.categoryId === this.currentCategory) {
          this.currentProducts.push(product);
        }
      });
    }
  }

  editProductDialog(productId: string, productName: string, categoryId: string, euroPrice: number) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        type: 'editProduct',
        title: 'Edit product...',
        text: 'Do you want to edit the name of the product?',
        textB: 'Which category does it belong to?',
        textC: 'What is its price?',
        inputAValue: productName,
        inputBValue: categoryId,
        inputCValue: euroPrice,
        productId,
        curr: this.prodSvc.defaultDialogCurrency(this.currentCurrency)
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== null && result !== undefined) {
        console.log(this.dialogWasClosed, result);
        let newEuroPrice = result[2];

        if (result[3] !== '€') {
          switch (result[3]) {
            case '$':
              newEuroPrice = this.prodSvc.convertToEuro(result[2], this.allCurrencies$.USD);
              break;

            case '£':
              newEuroPrice = this.prodSvc.convertToEuro(result[2], this.allCurrencies$.GBP);
              break;

            default:
              break;
          }
        }
        this.prodSvc.changeProductDetails(productId, result[0], result[1], newEuroPrice).subscribe(_ => {
          this.update.emit();
        });
      }

    });

  }

  deleteProductDialog(productId: string) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '300px',
      data: {
        type: 'deleteProduct',
        title: 'Delete Product...',
        text: 'Are you sure to delete this product?',
        productId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== null && result !== undefined) {
        console.log(this.dialogWasClosed, result);

        this.prodSvc.deleteProduct(result).subscribe(_ => {
          this.update.emit();
          console.log('Product deleted.');

        });
      }
    });
  }
}
