import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {PrdSvc, ProductsService} from 'src/app/services/products.service';

export interface DialogData {
  type: string;
  title: string;
  text: string;
  inputAValue?: string;
  inputBValue?: string;
  inputCValue?: string;
  textB?: string;
  textC?: string;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  providers: [ { provide: PrdSvc, useClass: ProductsService } ]
})
export class DialogComponent  {

  currentCategories;
  availableCurrencies = ['€', '$', '£'];
  selectedDialogCurrency = '€';

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private prodSvc: PrdSvc
  ) {
      this.prodSvc.getAllCategories().subscribe(result => {
        this.currentCategories = result;
      });
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
