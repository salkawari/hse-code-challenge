import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { faTimes, faEdit, faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import {PrdSvc, ProductsService} from 'src/app/services/products.service';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss'],
  providers: [ { provide: PrdSvc, useClass: ProductsService } ]
})
export class AsideComponent implements OnInit {
  @Input() allCategories$;
  @Output() currentCategory = new EventEmitter<string>();
  @Output() update: EventEmitter<null> = new EventEmitter();

  selectedCategory = 'all';

  // FA-Icons
  faTimes = faTimes;
  faPencilAlt = faPencilAlt;
  faEdit = faEdit;
  faTrashAlt = faTrashAlt;

  editMode = false;

  constructor(public dialog: MatDialog, private prodSvc: PrdSvc) { }

  ngOnInit() {
    this.selectCategory('all');
  }

  selectCategory(categoryId: string) {
    this.selectedCategory = categoryId;
    this.currentCategory.emit(categoryId);
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
  }

  editCategoryDialog(categoryId: string, categoryName: string) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        type: 'editCategory',
        title: 'Edit category...',
        text: 'Do you want to edit the name of the category?',
        inputAValue: categoryName,
        categoryId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== null && result !== undefined) {
        console.log('The dialog was closed', result);
        this.prodSvc.changeCategoryName(categoryId, result).subscribe(_ => {
          this.update.emit();
        });
      }

    });
  }

  deleteCategoryDialog(categoryId: string) {
    this.prodSvc.getAllProducts().subscribe(products => {
      let productsInCategory = false;
      (products as any).forEach(product => {
        if (product.categoryId === categoryId) {
          productsInCategory = true;
        }
      });
      if (productsInCategory) {
        const dialogRef = this.dialog.open(DialogComponent, {
          width: '400px',
          data: {
            type: 'deleteCategoryFailed',
            title: 'Can not be deleted...',
            text: 'There are still some products within this category.',
          }
        });

        dialogRef.afterClosed().subscribe(_ => {
          console.log('Can\'t be deleted.');
        });
      } else {
        const dialogRef = this.dialog.open(DialogComponent, {
          width: '300px',
          data: {
            type: 'deleteCategory',
            title: 'Delete Category...',
            text: 'Are you sure to delete this category?',
            categoryId
          }
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result !== null && result !== undefined) {
            console.log('The dialog was closed', result);

            this.prodSvc.deleteCategory(result).subscribe(x => {
              this.update.emit();
              console.log('Category deleted.');

            });
          }
        });
      }

    });
  }

}
