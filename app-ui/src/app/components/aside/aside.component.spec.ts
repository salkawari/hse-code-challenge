import { AsideComponent } from './aside.component';
import {MatDialog} from '@angular/material/dialog';
import {PrdSvc} from '../../services/products.service';
import { of } from 'rxjs';

class MyFakeDialogRef {
  txt: string;
  constructor(txt: string) {
    this.txt = txt;
  }
  afterClosed() {
    return of(this.txt);
  }
}
describe('AsideComponent', () => {

  let component: AsideComponent;

  const dialogSpy = jasmine.createSpyObj('MatDialog', ['open', 'close', 'afterClosed']);

  const cat1 = 'cat1';
  const cat2 = 'cat2';
  const prd1Cat1 = { prodName: 'prd1', categoryId: cat1 };

  it('editing categories leads to backend update category call', () => {
    const prdSvcSpy = getPrdSvcSpy();
    const newCatName = 'newCatName';

    const myDialogRef = new MyFakeDialogRef(newCatName);
    dialogSpy.open.and.returnValue(myDialogRef);

    prdSvcSpy.changeCategoryName.and.returnValue(of('edit was successful'));

    component = new AsideComponent(dialogSpy, prdSvcSpy);
    component.editCategoryDialog('categoryId1', newCatName);

    expect(prdSvcSpy.changeCategoryName.calls.count())
        .toBe(1, 'the category was updated');
  });

  it('we can delete cat2 as there are no products associated with this category', () => {

    const myDialogRef = new MyFakeDialogRef('we simulate that the dialog window is closed');
    dialogSpy.open.and.returnValue(myDialogRef);

    const prdSvcSpy = getPrdSvcSpy();
    const products = [prd1Cat1];
    prdSvcSpy.getAllProducts.and.returnValue(of(products));

    prdSvcSpy.deleteCategory.and.returnValue(of('something deleted'));

    component = new AsideComponent(dialogSpy, prdSvcSpy);
    component.deleteCategoryDialog(cat2);
    expect(prdSvcSpy.deleteCategory.calls.count())
        .toBe(1, 'cat2 was deleted successfully');
  });

  it('we cannot delete cat1 as there are products associated with this category', () => {

    const myDialogRef = new MyFakeDialogRef('we simulate that the dialog window is closed');
    dialogSpy.open.and.returnValue(myDialogRef);

    const prdSvcSpy = getPrdSvcSpy();
    const products = [prd1Cat1];
    prdSvcSpy.getAllProducts.and.returnValue(of(products));

    prdSvcSpy.deleteCategory.and.returnValue(of('something deleted'));

    component = new AsideComponent(dialogSpy, prdSvcSpy);
    component.deleteCategoryDialog(cat1);
    expect(prdSvcSpy.deleteCategory.calls.count())
        .toBe(0, 'cat1 doesnt get deleted as there is still a product associated');
  });

  function getPrdSvcSpy() {
    return jasmine.createSpyObj('PrdSvc',
        ['getAllProducts', 'deleteCategory', 'changeCategoryName']);
  }

});
