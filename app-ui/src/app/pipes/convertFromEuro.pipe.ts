import {Pipe, PipeTransform} from '@angular/core';
import { ProductsService } from '../services/products.service';

@Pipe({ name: 'convertfromeuro' })
export class ConvertFromEuroPipe implements PipeTransform {

    constructor(private productSvc: ProductsService) {}

    transform(euroPrice: number, exchangeRate: number): number {
        return this.productSvc.convertfromEuro(euroPrice, exchangeRate);
    }
}
