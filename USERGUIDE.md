# User Guide

## Starting Screen
Once you have started the application for the first time and open a browser 
at http://localhost:8080, you should be able to see the following:

![starting screen](user-guide-images/starting_screen.jpg)

----

## Creating a Category
To create a category, we click on the top right button called "Create Category". 
Now we can choose a name for this category and click on Ok.

![define_new_category_name](user-guide-images/define_new_category.jpg)

The category is now visible on the left hand side pane.

![added_cat_is_visible](user-guide-images/new_category_is_visible.jpg)

----

## Creating a Product
Now that we have a category, we can create a product. We click on the top right button
called "Create Product". After defining a product name and the price, we choose a category
from the dropdown category list. We save this new product by clicking on Ok.

![create_product](user-guide-images/create_product.jpg)

----

## Editing a Product
A product can be edited (see the yellow highlighted icon). 

![edit_product_p1](user-guide-images/edit_product_p1.jpg)

We change the name and price, then click on Ok.

![edit_product_p2](user-guide-images/edit_product_p2.jpg)

The product with the new details is visible.

![edit_product_p3](user-guide-images/edit_product_p3.jpg)

----

## Edit a Category
To edit categories, we click on the icon on the left next to "Categories".

![edit_cat_p1](user-guide-images/edit_categry_p1.jpg)

Now we change the name of the category and click on Ok.

![edit_category_part2](user-guide-images/edit_category_p2.jpg)

Now we can click on the X (on the left) to leave the category edit mode.

![edit_category_part3](user-guide-images/edit_category_p3.jpg)

This is what it looks like after we clicked on the X and left category edit mode.

![edit_category_part4](user-guide-images/edit_category_p4.jpg)

----

## View Products in Categories
We can see the products per category by clicking on the category on the left pane. Here
we clicked on the category entertainment. 

![prod_view_with_new_name](user-guide-images/view_products_for_given_category.jpg)

----

## View All Products for All Categories
Here we click on "All Products", on the left side, to see all Categories and all possible
Products.

![show_all](user-guide-images/view_all_products.png)

## Deleting a Product
To delete a product, we click on the bin symbol on the right side of the product.

![delete_product1](user-guide-images/deleting_product_p1.jpg)

We confirm the deletion by clicking Ok.

![delete_product2](user-guide-images/del_product_p2.jpg)

Now that product is no longer listed.

![delete_product3](user-guide-images/del_prod_p3.jpg)

----

## Deleting a Category
First we click on the pen icon on the right hand side next to Categories, this lets us
edit and delete categories.

![del_cat_p1](user-guide-images/del_cat_p1.jpg)

Now we can click on the bin icon (on the right) next to the category we want to delete.

![del_cat_p2](user-guide-images/del_cat_p2.jpg)

We cannot delete categories that still have products associated with them (a safety mechanism).

![del_cat_p3](user-guide-images/del_cat_p3.png)

Here we delete a category without associated products.
![del_cat_p4](user-guide-images/del_cat_p4.png)

----

## Setting the currency
By clicking on the currency dropdown (on the top of the screen), we can
change the displayed currency (there are 3 shown for brevity).

![change_currencies_p1](user-guide-images/change_currencies_for_all_p1.png)

Now we can see all the prices in dollars, for example. All prices are saved with the
price in euros, but the frontend can show them in different currencies.

![change_currencies_p2](user-guide-images/change_currencies_for_all_p2.png)

----

## Changing the price via the currency
We can also change the currency of a product. Here we are changing currency of the Games
Console product from dollars to sterling.

![edit_currency_p1](user-guide-images/edit_prod_change_currency_p1.png)

Now we can see the price changed in dollars changed for the games console. It was
300.99 dollars, then we changed it to be 300.99 pounds, which in dollars is 389.10 dollars.
![edit_currency_p1](user-guide-images/edit_prod_change_currency_p2.png)

----

