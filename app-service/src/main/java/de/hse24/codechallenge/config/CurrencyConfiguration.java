package de.hse24.codechallenge.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(CurrencyConfigurationProperties.class)
public class CurrencyConfiguration {

    private final CurrencyConfigurationProperties properties;

    public CurrencyConfiguration(CurrencyConfigurationProperties properties) {
        this.properties = properties;
    }

    @Bean
    public String key() {
        return properties.getKey();
    }

    @Bean
    public String url() {
        return properties.getUrl();
    }
}
