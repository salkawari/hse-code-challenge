package de.hse24.codechallenge.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("hse24.codechallenge.fixer")
public class CurrencyConfigurationProperties {

    private String key;
    private String url;
}
