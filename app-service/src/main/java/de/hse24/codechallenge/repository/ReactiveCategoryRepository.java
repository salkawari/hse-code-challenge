package de.hse24.codechallenge.repository;

import de.hse24.codechallenge.model.Category;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReactiveCategoryRepository extends ReactiveCrudRepository<Category, String> {
}