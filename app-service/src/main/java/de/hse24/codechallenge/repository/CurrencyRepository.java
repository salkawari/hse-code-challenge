package de.hse24.codechallenge.repository;

import de.hse24.codechallenge.model.Currency;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CurrencyRepository extends MongoRepository<Currency, Integer> {
}