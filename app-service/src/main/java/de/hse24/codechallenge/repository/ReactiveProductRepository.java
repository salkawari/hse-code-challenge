package de.hse24.codechallenge.repository;

import de.hse24.codechallenge.model.Product;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface ReactiveProductRepository extends ReactiveCrudRepository<Product, String> {
    Mono<Long> deleteByCategoryId(String categoryId);
}