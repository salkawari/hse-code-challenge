package de.hse24.codechallenge.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collections;
import java.util.Map;

@Value
@Document
public final class Currency {

    @Id
    private final Integer id;
    private final Long timestamp;

    @Getter(AccessLevel.NONE) // No Getter for collection, as we must return copy to ensure immutability
    private final Map<String, Float> rates;

    public Map<String, Float> getRates() {
        return rates == null ? Collections.emptyMap() : Map.copyOf(rates);
    }
}
