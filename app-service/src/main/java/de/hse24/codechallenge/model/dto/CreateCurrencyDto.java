package de.hse24.codechallenge.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class CreateCurrencyDto {
    private Boolean success;
    private Long timestamp;
    private String base;
    private Date date;

    private Map<String, Float> rates = new HashMap<>();

    private Map<String, String> error = new HashMap<>();
}

