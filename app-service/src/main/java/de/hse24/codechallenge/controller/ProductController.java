package de.hse24.codechallenge.controller;


import de.hse24.codechallenge.model.Product;
import de.hse24.codechallenge.model.dto.CreateProductDto;
import de.hse24.codechallenge.service.ReactiveService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("${hse24.codechallenge.paths.products}")
class ProductController {

    private final ReactiveService reactiveService;
    public ProductController(ReactiveService reactiveService) {
        this.reactiveService = reactiveService;
    }

    @GetMapping
    public Flux<Product> getAllProducts() {
        return reactiveService.getProducts();
    }

    @GetMapping("{productId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Mono<Product> getProduct(@PathVariable UUID productId) {
        return reactiveService.getProduct(productId.toString());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Mono<Product> addProduct(@RequestBody @Valid CreateProductDto createProductDto) {
        return reactiveService.addProduct(createProductDto);
    }

    @PutMapping("{productId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Mono<Product> updateProduct(@PathVariable UUID productId,
                              @RequestBody @Valid CreateProductDto updateProductDto) {
        return reactiveService.updateProduct(productId.toString(), updateProductDto);
    }

    @DeleteMapping("{productId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Mono<Void> deleteProduct(@PathVariable UUID productId) {
        return reactiveService.deleteProduct(productId.toString());
    }

}