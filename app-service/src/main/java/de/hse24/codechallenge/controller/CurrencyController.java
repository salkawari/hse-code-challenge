package de.hse24.codechallenge.controller;

import de.hse24.codechallenge.model.Currency;
import de.hse24.codechallenge.service.CurrencyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("${hse24.codechallenge.paths.currencies}")
public class CurrencyController {

    private final CurrencyService currencyService;

    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping
    public Currency getAllCurrencies() {
        return currencyService.calcCurrency();
    }
}
