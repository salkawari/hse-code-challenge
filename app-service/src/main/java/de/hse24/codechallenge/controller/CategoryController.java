package de.hse24.codechallenge.controller;


import de.hse24.codechallenge.model.Category;
import de.hse24.codechallenge.model.dto.CreateCategoryDto;
import de.hse24.codechallenge.service.ReactiveService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("${hse24.codechallenge.paths.categories}")
class CategoryController {

    private final ReactiveService reactiveService;
    public CategoryController(ReactiveService categoryService) {
        this.reactiveService = categoryService;
    }

    @GetMapping
    public Flux<Category> getAllCategories() {
        return reactiveService.getCategories();
    }

    @GetMapping("{categoryId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Mono<Category> getCategory(@PathVariable UUID categoryId) {
        return reactiveService.getCategory(categoryId.toString());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Mono<Category> addCategory(@RequestBody @Valid CreateCategoryDto createCategoryDto) {
        return reactiveService.addCategory(createCategoryDto);
    }


    @PutMapping("{categoryId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Mono<Category> updateCategory(@PathVariable UUID categoryId,
                               @RequestBody @Valid CreateCategoryDto updateCategoryDto) {
        return reactiveService.updateCategory(categoryId.toString(), updateCategoryDto);
    }

    @DeleteMapping("{categoryId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Mono<Void> deleteCategory(@PathVariable UUID categoryId) {
        return reactiveService.deleteCategory(categoryId.toString());
    }
}