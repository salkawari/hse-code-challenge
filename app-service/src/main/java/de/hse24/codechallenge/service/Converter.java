package de.hse24.codechallenge.service;

import de.hse24.codechallenge.model.Category;
import de.hse24.codechallenge.model.Product;
import de.hse24.codechallenge.model.dto.CreateCategoryDto;
import de.hse24.codechallenge.model.dto.CreateProductDto;
import lombok.experimental.UtilityClass;

@UtilityClass
final
class Converter {

    static Category toCategory(String categoryId, CreateCategoryDto createCategoryDto) {
        return Category.builder()
                .categoryId(categoryId)
                .categoryName(createCategoryDto.getCategoryName())
                .build();
    }

    static Product toProduct(String productId, CreateProductDto createProductDto) {
        return Product.builder()
                .productId(productId)
                .categoryId(createProductDto.getCategoryId())
                .productName(createProductDto.getProductName())
                .euroPrice(createProductDto.getEuroPrice())
                .build();
    }
}
