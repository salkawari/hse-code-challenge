package de.hse24.codechallenge.service;

import de.hse24.codechallenge.model.Category;
import de.hse24.codechallenge.model.Product;
import de.hse24.codechallenge.model.dto.CreateCategoryDto;
import de.hse24.codechallenge.model.dto.CreateProductDto;
import de.hse24.codechallenge.repository.ReactiveCategoryRepository;
import de.hse24.codechallenge.repository.ReactiveProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static de.hse24.codechallenge.service.Converter.toCategory;
import static de.hse24.codechallenge.service.Converter.toProduct;

@Slf4j
@Service
public class ReactiveService {

    private final ReactiveCategoryRepository reactiveCategoryRepository;
    private final ReactiveProductRepository reactiveProductRepository;

    public ReactiveService(ReactiveCategoryRepository reactiveCategoryRepository,
                           ReactiveProductRepository reactiveProductRepository) {
        this.reactiveCategoryRepository = reactiveCategoryRepository;
        this.reactiveProductRepository = reactiveProductRepository;
    }

    public Mono<Category> addCategory(CreateCategoryDto createCategoryDto) {
        Category category = toCategory(UUID.randomUUID().toString(), createCategoryDto);
        return reactiveCategoryRepository.save(category);
    }

    public Mono<Category> updateCategory(String categoryId, CreateCategoryDto updateCategoryDto) {
        return reactiveCategoryRepository.findById(categoryId).flatMap(c -> {
            Category category = toCategory(categoryId, updateCategoryDto);

            return reactiveCategoryRepository.save(category);
        });
    }

    public Mono<Void> deleteCategory(String categoryId) {
        return reactiveCategoryRepository.findById(categoryId)
                .flatMap(c -> reactiveCategoryRepository.deleteById(categoryId))
                .doFinally(x -> reactiveProductRepository.deleteByCategoryId(categoryId)
                        .subscribe(done -> log.debug("deleted products with categoryId {}", categoryId)));
    }

    public Flux<Category> getCategories() {
        return reactiveCategoryRepository.findAll();
    }

    public Mono<Category> getCategory(String categoryId) {
        return reactiveCategoryRepository.findById(categoryId);
    }

    public Mono<Product> addProduct(CreateProductDto createProductDto) {
        Product product = toProduct(UUID.randomUUID().toString(), createProductDto);
        return reactiveProductRepository.save(product);
    }

    public Mono<Product> updateProduct(String productId, CreateProductDto updateProductDto) {
        return reactiveProductRepository.findById(productId).flatMap(c -> {
            Product product = toProduct(productId, updateProductDto);

            return reactiveProductRepository.save(product);
        });
    }

    public Flux<Product> getProducts() {
        return reactiveProductRepository.findAll();
    }

    public Mono<Product> getProduct(String productId) {
        return reactiveProductRepository.findById(productId);
    }

    public Mono<Void> deleteProduct(String productId) {
        return reactiveProductRepository.findById(productId)
                .flatMap(c -> reactiveProductRepository.deleteById(productId));
    }

}
