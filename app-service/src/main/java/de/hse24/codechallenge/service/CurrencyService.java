package de.hse24.codechallenge.service;

import de.hse24.codechallenge.config.CurrencyConfiguration;
import de.hse24.codechallenge.model.Currency;
import de.hse24.codechallenge.model.dto.CreateCurrencyDto;
import de.hse24.codechallenge.repository.CurrencyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.Instant;
import java.util.Collections;

@Slf4j
@Service
public class CurrencyService {

    static final String ACCESS_KEY = "access_key";
    static final String ERROR_CODE_KEY = "code";
    static final String ERROR_INFO_KEY = "info";

    static final Integer ID = 1;
    private static final Long HOUR = 60 * 60L;

    private static final Currency EMPTY = new Currency(ID, 1L, Collections.emptyMap());

    private final RestTemplate restTemplate;

    private final CurrencyRepository currencyRepository;

    private String uri;

    public CurrencyService(RestTemplate restTemplate, CurrencyRepository currencyRepo, CurrencyConfiguration config) {
        this.restTemplate = restTemplate;
        this.currencyRepository = currencyRepo;
        uri = UriComponentsBuilder.fromUriString(config.url()).queryParam(ACCESS_KEY, config.key()).toUriString();
    }

    public Currency calcCurrency() {
        Currency oldCurrency = retrieveCurrent();
        if (isTooOld(oldCurrency.getTimestamp())) {
            refreshCurrency();
        }

        return oldCurrency;
    }


    private Currency retrieveCurrent() {
        return currencyRepository.findById(ID).orElse(EMPTY);
    }


    private void refreshCurrency() {
        try {
            CreateCurrencyDto currencyDto = restTemplate.getForObject(uri, CreateCurrencyDto.class);

            if (currencyDto == null) {
                log.warn("refreshCurrency - currencyDto is null");
                return;
            }

            if (Boolean.TRUE.equals(currencyDto.getSuccess())) {
                Currency currency = toCurrency(currencyDto);
                log.debug("refreshCurrency - saving on success");
                currencyRepository.save(currency);

            } else {
                log.warn("Unable to retrieve currency: {}, {}", currencyDto.getError().get(ERROR_CODE_KEY),
                        currencyDto.getError().get(ERROR_INFO_KEY));
            }

        } catch (ResourceAccessException e) {
            log.warn("Unable to reach fixer server", e);
        }
    }

    private Currency toCurrency(CreateCurrencyDto currencyDto) {
        return new Currency(ID, currencyDto.getTimestamp(), currencyDto.getRates());
    }

    private boolean isTooOld(Long timestamp) {
        return (timestamp + HOUR) < Instant.now().getEpochSecond();
    }

}
