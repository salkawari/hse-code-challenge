package de.hse24.codechallenge.controller;

import de.hse24.codechallenge.model.Currency;
import de.hse24.codechallenge.service.CurrencyService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class CurrencyControllerTest {

    private static final Currency CURRENCY = new Currency(1, 1L, new HashMap<>());
    @Mock
    private CurrencyService currencyService;

    @InjectMocks
    private CurrencyController currencyController;

    @Test
    void Should_ReturnAllCurrencies_WhenCallingGetAllCurrencies() {
        when(currencyService.calcCurrency()).thenReturn(CURRENCY);
        currencyController.getAllCurrencies();
        assertEquals(CURRENCY, currencyController.getAllCurrencies());
    }
}
