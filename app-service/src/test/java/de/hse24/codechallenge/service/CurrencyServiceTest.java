package de.hse24.codechallenge.service;

import de.hse24.codechallenge.config.CurrencyConfiguration;
import de.hse24.codechallenge.model.Currency;
import de.hse24.codechallenge.model.dto.CreateCurrencyDto;
import de.hse24.codechallenge.repository.CurrencyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;

import static de.hse24.codechallenge.service.CurrencyService.ACCESS_KEY;
import static de.hse24.codechallenge.service.CurrencyService.ERROR_CODE_KEY;
import static de.hse24.codechallenge.service.CurrencyService.ERROR_INFO_KEY;
import static de.hse24.codechallenge.service.CurrencyService.ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class CurrencyServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private CurrencyRepository currencyRepository;

    @Mock
    private CurrencyConfiguration fixerConfiguration;

    private static final String GET_URL = "?" + ACCESS_KEY + "=";

    private static final Map<String, String> ERROR = Map.of(ERROR_CODE_KEY, "101", ERROR_INFO_KEY, "some msg");

    private static final Long NOW = Instant.now().getEpochSecond();
    private static final Long OLD = Instant.MIN.getEpochSecond();

    private static final CreateCurrencyDto BAD_CURRENCY_DTO =
            CreateCurrencyDto.builder()
                    .success(false)
                    .error(ERROR)
                    .timestamp(NOW)
                    .build();

    private static final Map<String, Float> RATES1 = Map.of("USD", 1.2f, "GBP", 1.1f);

    private static final CreateCurrencyDto GOOD_CURRENCY_NEW_DTO = CreateCurrencyDto.builder()
            .success(true)
            .timestamp(NOW)
            .rates(RATES1)
            .build();

    private static final Map<String, Float> RATES2 = Map.of("WAN", 2.2f, "LEI", 2.1f);

    private static final Currency OLD_CURRENCY = new Currency(ID, OLD, RATES2);

    private static final Currency NEW_CURRENCY = toGoodNewCurrency();

    private CurrencyService currencyService;

    @BeforeEach
    void setup() {
        when(fixerConfiguration.key()).thenReturn("");
        when(fixerConfiguration.url()).thenReturn("");

        currencyService = new CurrencyService(restTemplate, currencyRepository, fixerConfiguration);
    }

    @Test
    void Should_NotChangeAnything_When_GivenNullAsCategoryDto() {
        when(restTemplate.getForObject(GET_URL, CreateCurrencyDto.class)).thenReturn(null);
        currencyService.calcCurrency();
        verify(currencyRepository, never()).save(any());
    }

    @Test
    void Should_NotSave_When_GivenBadCurrency() {

        when(restTemplate.getForObject(GET_URL, CreateCurrencyDto.class)).thenReturn(BAD_CURRENCY_DTO);
        currencyService.calcCurrency();
        verify(currencyRepository, never()).save(any());
    }


    @Test
    void Should_NotSave_When_GivenNewCurrencyAsExistingCurrencyIsAlreadyFreshEnough() {
        when(currencyRepository.findById(ID)).thenReturn(Optional.of(NEW_CURRENCY));
        when(restTemplate.getForObject(GET_URL, CreateCurrencyDto.class)).thenReturn(GOOD_CURRENCY_NEW_DTO);
        currencyService.calcCurrency();
        verify(currencyRepository, never()).save(any());
    }

    @Test
    void Should_Save_When_GivenGoodCurrencyWithEmptyRepo() {
        when(restTemplate.getForObject(GET_URL, CreateCurrencyDto.class)).thenReturn(GOOD_CURRENCY_NEW_DTO);
        currencyService.calcCurrency();
        verify(currencyRepository, times(1)).save(NEW_CURRENCY);
    }

    @Test
    void Should_Save_When_GivenGoodCurrencyWithStaleRepo() {
        when(currencyRepository.findById(ID)).thenReturn(Optional.of(OLD_CURRENCY));
        when(restTemplate.getForObject(GET_URL, CreateCurrencyDto.class)).thenReturn(GOOD_CURRENCY_NEW_DTO);
        currencyService.calcCurrency();
        verify(currencyRepository, times(1)).save(NEW_CURRENCY);
    }

    @Test
    void Should_KeepOldEntry_When_ExceptionDuringRemoteGetCurrency() {
        when(currencyRepository.findById(ID)).thenReturn(Optional.of(OLD_CURRENCY));
        when(restTemplate.getForObject(GET_URL, CreateCurrencyDto.class))
                .thenThrow(ResourceAccessException.class);
        Assertions.assertEquals(OLD_CURRENCY, currencyService.calcCurrency());
    }

    private static Currency toGoodNewCurrency() {
        return new Currency(ID, CurrencyServiceTest.GOOD_CURRENCY_NEW_DTO.getTimestamp(),
                CurrencyServiceTest.GOOD_CURRENCY_NEW_DTO.getRates());
    }
}
