package de.hse24.codechallenge.integration;

import de.hse24.codechallenge.model.Category;
import de.hse24.codechallenge.model.Product;
import de.hse24.codechallenge.model.dto.CreateCategoryDto;
import de.hse24.codechallenge.model.dto.CreateProductDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.UUID;

import static org.awaitility.Awaitility.await;

@Slf4j
@EnableAutoConfiguration
@ExtendWith(SpringExtension.class)
class ProductIT extends AbstractReactiveIT {

    private CreateProductDto createProductDto;
    private Category category;

    @BeforeEach
    void setup() throws IOException {
        super.setup();
        CreateCategoryDto createCategoryDto = CreateCategoryDto.builder()
                .categoryName(UUID.randomUUID().toString())
                .build();

        category = client.postCreateCategoryDto(createCategoryDto);

        createProductDto = CreateProductDto.builder()
                .productName(UUID.randomUUID().toString())
                .categoryId(category.getCategoryId())
                .euroPrice(1f)
                .build();
    }

    @Test
    void Should_PersistProduct_When_SentCreateProductDto() {
        Product existingProduct = client.postCreateProductDto(createProductDto);
        checkProductIdExists(existingProduct.getProductId());
        checkProductNameIsFound(existingProduct.getProductName());
        checkProductCount(1);
    }

    @Test
    void Should_NotPersistProduct_When_SentUpdateForNonExistingProduct() {
        client.postCreateProductDto(createProductDto);
        checkProductCount(1);

        String unusedProductId = UUID.randomUUID().toString();

        CreateProductDto newCreateProductDto = createProductDtoWithNewProductName(createProductDto.getCategoryId());

        client.updateProduct(unusedProductId, newCreateProductDto);

        checkProductNameIsFound(createProductDto.getProductName());
        checkProductCount(1);
    }

    @Test
    void Should_UpdateProduct_When_SentUpdateExistingProduct() {
        Product existingProduct = client.postCreateProductDto(createProductDto);

        CreateProductDto updatedProductDto = createProductDtoWithNewProductName(category.getCategoryId());
        client.updateProduct(existingProduct.getProductId(), updatedProductDto);

        checkProductNameIsFound(updatedProductDto.getProductName());
        checkProductIdExists(existingProduct.getProductId());

        checkProductCount(1);
    }

    @Test
    void Should_DeleteProduct_When_SentCreateProductThenDeleteProduct() {
        Product existingProduct = client.postCreateProductDto(createProductDto);
        checkProductCount(1);

        client.deleteProduct(existingProduct.getProductId());
        checkProductCount(0);
    }

    private CreateProductDto createProductDtoWithNewProductName(String categoryId) {
        return CreateProductDto.builder()
                .productName(UUID.randomUUID().toString())
                .categoryId(categoryId)
                .euroPrice(1f)
                .build();
    }

    private void checkProductNameIsFound(String productName) {
        await().untilAsserted(() ->
                client.getAllProducts()
                        .stream()
                        .filter(c -> c.getProductName().equals(productName))
                        .findAny()
                        .orElseThrow(AssertionError::new));
    }
}
