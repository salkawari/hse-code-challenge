package de.hse24.codechallenge.integration;

import de.hse24.codechallenge.model.Category;
import de.hse24.codechallenge.model.Product;
import de.hse24.codechallenge.model.dto.CreateCategoryDto;
import de.hse24.codechallenge.model.dto.CreateProductDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.UUID;

import static org.awaitility.Awaitility.await;

@Slf4j
@EnableAutoConfiguration
@ExtendWith(SpringExtension.class)
class CategoryIT extends AbstractReactiveIT {

    private CreateCategoryDto createCategoryDto;

    @BeforeEach
    void setup() throws IOException {
        super.setup();
        createCategoryDto = CreateCategoryDto.builder().categoryName(UUID.randomUUID().toString()).build();
    }

    @Test
    void Should_PersistCategory_When_SentCreateCategoryDto() {
        Category category = client.postCreateCategoryDto(createCategoryDto);

        checkCategoryIdExists(category.getCategoryId());
        checkCategoryNameIsFound(category.getCategoryName());
        checkCategoryCount(1);
    }

    @Test
    void Should_NotPersistCategory_When_SentUpdateForNonExistingCategory() {

        String unusedCategoryId = UUID.randomUUID().toString();

        client.updateCategory(unusedCategoryId, createCategoryDto);

        checkCategoryCount(0);
    }

    @Test
    void Should_UpdateCategory_When_SentUpdateExistingCategory() {

        Category existingCategory = client.postCreateCategoryDto(createCategoryDto);

        CreateCategoryDto updatedCategoryDto = createCategoryDtoWithNewCategoryName();

        client.updateCategory(existingCategory.getCategoryId(), updatedCategoryDto);

        checkCategoryNameIsFound(updatedCategoryDto.getCategoryName());
        checkCategoryIdExists(existingCategory.getCategoryId());

        checkCategoryCount(1);
    }

    @Test
    void Should_DeleteCategory_When_SentCreateCategoryThenDeleteCategory() {
        Category existingCategory = client.postCreateCategoryDto(createCategoryDto);
        client.deleteCategory(existingCategory.getCategoryId());
        checkCategoryCount(0);
    }

    @Test
    void Should_DeleteCategoryAndItsProducts_When_SentDeleteCategory() {

        Category existingCategory = client.postCreateCategoryDto(createCategoryDto);

        CreateProductDto createProductDto = CreateProductDto.builder()
                .euroPrice(1f)
                .productName("x")
                .categoryId(existingCategory.getCategoryId())
                .build();

        Product existingProduct = client.postCreateProductDto(createProductDto);

        checkProductIdExists(existingProduct.getProductId());
        checkProductCount(1);

        checkCategoryIdExists(existingCategory.getCategoryId());
        checkCategoryCount(1);

        client.deleteCategory(existingCategory.getCategoryId());
        checkProductCount(0);
        checkCategoryCount(0);
    }

    private CreateCategoryDto createCategoryDtoWithNewCategoryName() {
        return CreateCategoryDto.builder().categoryName(UUID.randomUUID().toString()).build();
    }

    private void checkCategoryNameIsFound(String categoryName) {
        await().untilAsserted(() ->
                client.getAllCategories()
                        .stream()
                        .filter(c -> c.getCategoryName().equals(categoryName))
                        .findAny()
                        .orElseThrow(AssertionError::new));
    }
}
