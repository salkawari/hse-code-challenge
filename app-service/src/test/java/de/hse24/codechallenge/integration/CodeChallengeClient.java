package de.hse24.codechallenge.integration;


import de.hse24.codechallenge.model.Category;
import de.hse24.codechallenge.model.Product;
import de.hse24.codechallenge.model.dto.CreateCategoryDto;
import de.hse24.codechallenge.model.dto.CreateProductDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

public class CodeChallengeClient {

    private static final ParameterizedTypeReference<List<Category>> CATEGORIES_LIST_TYPE =
            new ParameterizedTypeReference<>() {
            };

    private static final ParameterizedTypeReference<List<Product>> PRODUCTS_LIST_TYPE =
            new ParameterizedTypeReference<>() {
            };

    private static final String URL_TEMPLATE = "http://%s:%d%s";
    private static final String LOCALHOST = "localhost";

    private final RestTemplate restTemplate = new RestTemplate();

    private HttpHeaders headers = new HttpHeaders();
    private HttpEntity<Map<String, String>> entity = new HttpEntity<>(null, headers);

    private int serverPort;

    @Value("${hse24.codechallenge.paths.categories}")
    private String categoriesPath;

    @Value("${hse24.codechallenge.paths.products}")
    private String productsPath;

    void setLocalServerPort(int localServerPort) {
        this.serverPort = localServerPort;
    }

    Category postCreateCategoryDto(CreateCategoryDto createCategoryDto) {
        return restTemplate.postForEntity(buildUrl(categoriesPath), createCategoryDto, Category.class).getBody();
    }

    void updateCategory(String categoryId, CreateCategoryDto updateCategory) {
        restTemplate.put(buildUrl(categoriesPath, categoryId), updateCategory);
    }

    void deleteCategory(String categoryId) {
        restTemplate.delete(buildUrl(categoriesPath, categoryId));
    }

    List<Category> getAllCategories() {
        return restTemplate.exchange(buildUrl(categoriesPath), HttpMethod.GET, entity, CATEGORIES_LIST_TYPE).getBody();
    }

    ResponseEntity<Category> getCategory(String categoryId) {
        return restTemplate.getForEntity(buildUrl(categoriesPath, categoryId), Category.class);
    }

    Product postCreateProductDto(CreateProductDto createProductDto) {
        return restTemplate.postForEntity(buildUrl(productsPath), createProductDto, Product.class).getBody();
    }

    void updateProduct(String productId, CreateProductDto updateProduct) {
        restTemplate.put(buildUrl(productsPath, productId), updateProduct);
    }

    void deleteProduct(String productId) {
        restTemplate.delete(buildUrl(productsPath, productId));
    }

    List<Product> getAllProducts() {
        return restTemplate.exchange(buildUrl(productsPath), HttpMethod.GET, entity, PRODUCTS_LIST_TYPE).getBody();
    }

    ResponseEntity<Product> getProduct(String productId) {
        return restTemplate.getForEntity(buildUrl(productsPath, productId), Product.class);
    }

    private String buildUrl(String path, String... pathItems) {
        StringBuilder urlBuilder = new StringBuilder(String.format(URL_TEMPLATE, LOCALHOST, serverPort, path));

        for (String pathItem : pathItems) {
            urlBuilder.append('/')
                    .append(pathItem);
        }

        return urlBuilder.toString();
    }
}
