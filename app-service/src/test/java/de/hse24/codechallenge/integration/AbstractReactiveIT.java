package de.hse24.codechallenge.integration;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import de.hse24.codechallenge.Application;
import de.hse24.codechallenge.repository.ReactiveCategoryRepository;
import de.hse24.codechallenge.repository.ReactiveProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

@Slf4j
@ActiveProfiles("it")
@ContextConfiguration(classes = {
        Application.class,
        CodeChallengeClient.class
})
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractReactiveIT {

    private static final int TIMEOUT = 10;

    @Autowired
    public CodeChallengeClient client;

    @LocalServerPort
    public int serverPort;


    @BeforeAll
    static void initSetup() {
        Awaitility.setDefaultTimeout(TIMEOUT, TimeUnit.SECONDS);
    }

    private MongodExecutable mongodExecutable;

    @Autowired
    private ReactiveProductRepository productRepository;

    @Autowired
    private ReactiveCategoryRepository categoryRepository;

    @BeforeEach
    void setup() throws IOException {
        String ip = "localhost";
        int port = 27017;

        IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
                .net(new Net(ip, port, Network.localhostIsIPv6()))
                .build();

        MongodStarter starter = MongodStarter.getDefaultInstance();
        mongodExecutable = starter.prepare(mongodConfig);
        mongodExecutable.start();

        client.setLocalServerPort(serverPort);

        productRepository.deleteAll().subscribe(x -> log.debug("purged productRepository"));
        categoryRepository.deleteAll().subscribe(x -> log.debug("purged categoryRepository"));
        checkProductCount(0);
        checkCategoryCount(0);
    }

    @AfterEach
    void clean() {
        mongodExecutable.stop();
    }

    void checkProductIdExists(String productId) {
        await().untilAsserted(() ->
                Assertions.assertTrue(client.getProduct(productId).hasBody())
        );
    }

    void checkCategoryIdExists(String categoryId) {
        await().untilAsserted(() ->
                Assertions.assertTrue(client.getCategory(categoryId).hasBody())
        );
    }

    void checkProductCount(int count) {
        await().untilAsserted(() -> Assertions.assertEquals(count, client.getAllProducts().size()));
    }

    void checkCategoryCount(int count) {
        await().untilAsserted(() -> Assertions.assertEquals(count, client.getAllCategories().size()));
    }
}

